package br.dev.aao.syscertameapi.test.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import br.dev.aao.syscertameapi.endereco.Estado;
import br.dev.aao.syscertameapi.mapper.CandidatoMapper;
import br.dev.aao.syscertameapi.usuario.Candidato;
import br.dev.aao.syscertameapi.usuario.DenominacaoEtnica;
import br.dev.aao.syscertameapi.usuario.Genero;
import br.dev.aao.syscertameapi.usuario.dto.CandidatoDTO;


public class CandidatoMapperTest {
    public CandidatoDTO criaCandidatoDTO(){
        String nome = "Nome Teste 1";
        LocalDate nascimento = LocalDate.of(2000, 01, 04); 
        String cpf = "123.456.789-00";
        String emailPrincipal = nome.replace(" ", ".").concat("@company.org");
        String telefonePrincipal = "(00) 12345-6789";
        String rua = "Rua Teste 1"; 
        String numero = "99999";
        String bairro = "Bairro Teste 1"; 
        String complemento = "Complemento Teste 1";
        String cidade = "Cidade Teste 1";
        Estado estado = Estado.ACRE; 
        String CEP = "12345-6789";
        Genero genero = Genero.NAO_IDENTIFICAR;
        DenominacaoEtnica cor = DenominacaoEtnica.AMARELO;
        LocalDate dataCadastro = LocalDate.now();
        Long numeroNIS = 1234567890L;
        boolean doadorSangue = false;
        boolean doadorMedula = true;
        boolean trabalhoEleitoral = true;
        boolean jurado = false;
        return new CandidatoDTO(nome, nascimento, cpf, emailPrincipal, telefonePrincipal, rua, numero, bairro, complemento, cidade, estado, CEP, genero, cor, dataCadastro, numeroNIS, doadorSangue, doadorMedula, trabalhoEleitoral, jurado);
    }
    
    @Test
	void MapeamentoCadidatoParaCanditatoDtoTest() {
        CandidatoDTO candandidatoDTO = criaCandidatoDTO();
        Candidato candidato = CandidatoMapper.INSTANCE.map(candandidatoDTO);
        assertEquals(candandidatoDTO.getNome(), candidato.getNome());
        assertEquals(candandidatoDTO.getNascimento(), candidato.getNascimento());
        assertEquals(candandidatoDTO.getCpf(), candidato.getCpf());
        assertEquals(candandidatoDTO.getEmailPrincipal(), candidato.getEmailPrincipal());
        assertEquals(candandidatoDTO.getTelefonePrincipal(), candidato.getTelefonePrincipal());
        assertEquals(candandidatoDTO.getRua(), candidato.getEnderecoPrincipal().getRua());
        assertEquals(candandidatoDTO.getNumero(), candidato.getEnderecoPrincipal().getNumero());
        assertEquals(candandidatoDTO.getComplemento(), candidato.getEnderecoPrincipal().getComplemento());
        assertEquals(candandidatoDTO.getEstado(), candidato.getEnderecoPrincipal().getEstado());
        assertEquals(candandidatoDTO.getCEP(), candidato.getEnderecoPrincipal().getCEP());
        assertEquals(candandidatoDTO.getGenero(), candidato.getGenero());
        assertEquals(candandidatoDTO.getCor(), candidato.getCor());
        assertEquals(candandidatoDTO.getDataCadastro(), candidato.getDataCadastro());
        assertEquals(candandidatoDTO.getNumeroNIS(), candidato.getNumeroNIS());
        assertEquals(candandidatoDTO.isDoadorSangue(), candidato.isDoadorSangue());
        assertEquals(candandidatoDTO.isDoadorMedula(), candidato.isDoadorMedula());
        assertEquals(candandidatoDTO.isJurado(), candidato.isJurado());
    }
}
