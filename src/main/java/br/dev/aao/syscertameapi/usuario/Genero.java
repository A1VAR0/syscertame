package br.dev.aao.syscertameapi.usuario;

public enum Genero {
    MASCULINO,
    FEMININO,
    NAO_IDENTIFICAR
}
