package br.dev.aao.syscertameapi.usuario.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.dev.aao.syscertameapi.usuario.Candidato;

@Repository
public interface CandidatoRepository extends JpaRepository<Candidato, Long>{

}
