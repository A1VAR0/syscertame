package br.dev.aao.syscertameapi.usuario;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity
public class Candidato extends Pessoa{


    /**
     * 
     */
    @Column(nullable = false, columnDefinition = "DATE")
    private LocalDate dataCadastro;

    /**
     * 
     */
    @Column(nullable = true)
    private Long numeroNIS;

    /**
     * 
     */
    @Column(name = "doador_sangue", nullable = true)
    private boolean doadorSangue;

    /**
     * 
     */
    @Column(name = "doador_medula", nullable = true)
    private boolean doadorMedula;

    /**
     * 
     */
    @Column(name = "trabalho_eleitoral", nullable = true)
    private boolean trabalhoEleitoral;

    /**
     * 
     */
    @Column(nullable = true)
    private boolean jurado;

    public LocalDate getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(LocalDate dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Long getNumeroNIS() {
        return numeroNIS;
    }

    public void setNumeroNIS(Long numeroNIS) {
        this.numeroNIS = numeroNIS;
    }

    public boolean isDoadorSangue() {
        return doadorSangue;
    }

    public void setDoadorSangue(boolean doadorSangue) {
        this.doadorSangue = doadorSangue;
    }

    public boolean isDoadorMedula() {
        return doadorMedula;
    }

    public void setDoadorMedula(boolean doadorMedula) {
        this.doadorMedula = doadorMedula;
    }

    public boolean isTrabalhoEleitoral() {
        return trabalhoEleitoral;
    }

    public void setTrabalhoEleitoral(boolean trabalhoEleitoral) {
        this.trabalhoEleitoral = trabalhoEleitoral;
    }

    public boolean isJurado() {
        return jurado;
    }

    public void setJurado(boolean jurado) {
        this.jurado = jurado;
    }

    
}
