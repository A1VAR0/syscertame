
package br.dev.aao.syscertameapi.usuario.dto;

import java.time.LocalDate;

import br.dev.aao.syscertameapi.endereco.Estado;
import br.dev.aao.syscertameapi.usuario.DenominacaoEtnica;
import br.dev.aao.syscertameapi.usuario.Genero;

public record CandidatoRecordDTO(String nome, LocalDate nascimento, String cpf, String emailPrincipal,
        String telefonePrincipal, String rua, Integer numero, String bairro, String complemento, String cidade,
        Estado estado, String CEP,
        Genero genero, DenominacaoEtnica cor, LocalDate dataCadastro, Long numeroNIS, boolean doadorSangue,
        boolean doadorMedula, boolean trabalhoEleitoral, boolean jurado) {

}
