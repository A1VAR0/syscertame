package br.dev.aao.syscertameapi.usuario;

import java.io.Serializable;
import java.time.LocalDate;

import br.dev.aao.syscertameapi.endereco.Endereco;
import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Pessoa implements Serializable{
    private static final long serialVersionUID = 1L;
     /**
     * 
     */
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    /**
     * Nome e sobrenome
     */
    @Column(nullable = false, length = 200)
    private String nome;

    /**
     * dia, mês e ano dd/mm/yyyy
     */
    @Column(nullable = false, columnDefinition = "DATE")
    private LocalDate nascimento;

    /**
     * formato nnn.nnn.nnn-nn
     */
    @Column(nullable = false, unique = true, length = 14 )
    private String cpf;

    /**
     * 
     */
    @Column(nullable = false, length = 200)
    private String emailPrincipal;

    /**
     * formato (nn) nnnnn-nnnn
     */
    @Column(nullable = false, length = 15)
    private String telefonePrincipal;

    /**
     * 
     */
    @Embedded
    private Endereco enderecoPrincipal;

    /**
     * 
     */
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Genero genero;

    /**
     * 
     */
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private DenominacaoEtnica cor;

    
    public Pessoa() {
        this.enderecoPrincipal = new Endereco();
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getNascimento() {
        return nascimento;
    }

    public void setNascimento(LocalDate nascimento) {
        this.nascimento = nascimento;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmailPrincipal() {
        return emailPrincipal;
    }

    public void setEmailPrincipal(String emailPrincipal) {
        this.emailPrincipal = emailPrincipal;
    }

    public String getTelefonePrincipal() {
        return telefonePrincipal;
    }

    public void setTelefonePrincipal(String telefonePrincipal) {
        this.telefonePrincipal = telefonePrincipal;
    }

    public Endereco getEnderecoPrincipal() {
        return enderecoPrincipal;
    }

    public void setEnderecoPrincipal(Endereco enderecoPrincipal) {
        this.enderecoPrincipal = enderecoPrincipal;
    }

    public Genero getGenero() {
        return genero;
    }

    public void setGenero(Genero genero) {
        this.genero = genero;
    }

    public DenominacaoEtnica getCor() {
        return cor;
    }

    public void setCor(DenominacaoEtnica cor) {
        this.cor = cor;
    }

    @Override
    public String toString() {
        return super.toString();
    }
    
}
