package br.dev.aao.syscertameapi.usuario.repository.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.dev.aao.syscertameapi.exception.RecursoNaoEncontradoException;
import br.dev.aao.syscertameapi.mapper.CandidatoMapper;
import br.dev.aao.syscertameapi.usuario.Candidato;
import br.dev.aao.syscertameapi.usuario.dto.CandidatoDTO;
import br.dev.aao.syscertameapi.usuario.repository.CandidatoRepository;

@Service
public class CandidatoService {
    @Autowired
    private CandidatoRepository candidatoRepository;

    public Candidato cadastrar(CandidatoDTO candidatoDTO){
        var candidato = CandidatoMapper.INSTANCE.map(candidatoDTO);
        return candidatoRepository.save(candidato);
    }

    public List<Candidato> buscarTodos(){
        return candidatoRepository.findAll();
    }

    public Candidato buscarPorId(Long id){
        return candidatoRepository.findById(id).orElseThrow(() -> new RecursoNaoEncontradoException("O candidato não foi encontrado!"));
    }

    public void deletarCandidato(Long id){
        var deletarCandidato = candidatoRepository.findById(id).orElseThrow(() -> new RecursoNaoEncontradoException("O candidato não foi encontrado!"));
        candidatoRepository.delete(deletarCandidato);
    }

    public Candidato atualizarCandidato(Long id, CandidatoDTO candidatoAtualizacao){
        var candidato = candidatoRepository.findById(id).orElseThrow(() -> new RecursoNaoEncontradoException("O candidato não foi encontrado!"));
        candidato.setCor(candidatoAtualizacao.getCor());
        candidato.setCpf(candidatoAtualizacao.getCpf());
        candidato.setDataCadastro(candidatoAtualizacao.getDataCadastro());
        candidato.setDoadorMedula(candidatoAtualizacao.isDoadorMedula());
        candidato.setDoadorSangue(candidatoAtualizacao.isDoadorSangue());
        candidato.setEmailPrincipal(candidatoAtualizacao.getEmailPrincipal());
        candidato.getEnderecoPrincipal().setBairro(candidatoAtualizacao.getBairro());
        candidato.getEnderecoPrincipal().setCEP(candidatoAtualizacao.getCEP());
        candidato.getEnderecoPrincipal().setCidade(candidatoAtualizacao.getCidade());
        candidato.getEnderecoPrincipal().setComplemento(candidatoAtualizacao.getComplemento());
        candidato.getEnderecoPrincipal().setEstado(candidatoAtualizacao.getEstado());
        candidato.getEnderecoPrincipal().setNumero(candidatoAtualizacao.getNumero());
        candidato.getEnderecoPrincipal().setRua(candidatoAtualizacao.getRua());
        candidato.setGenero(candidatoAtualizacao.getGenero());
        candidato.setJurado(candidatoAtualizacao.isJurado());
        candidato.setNascimento(candidatoAtualizacao.getNascimento());
        candidato.setNome(candidatoAtualizacao.getNome());
        candidato.setNumeroNIS(candidatoAtualizacao.getNumeroNIS());
        candidato.setTelefonePrincipal(candidatoAtualizacao.getTelefonePrincipal());
        candidato.setTrabalhoEleitoral(candidatoAtualizacao.isTrabalhoEleitoral());
        return candidatoRepository.save(candidato);
    }

}
