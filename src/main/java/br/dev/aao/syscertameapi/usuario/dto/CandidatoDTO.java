package br.dev.aao.syscertameapi.usuario.dto;

import java.time.LocalDate;

import br.dev.aao.syscertameapi.endereco.Estado;
import br.dev.aao.syscertameapi.usuario.DenominacaoEtnica;
import br.dev.aao.syscertameapi.usuario.Genero;

public class CandidatoDTO {
        private String nome;
        private LocalDate nascimento;
        private String cpf;
        private String emailPrincipal;
        private String telefonePrincipal;
        private String rua;
        private String numero;
        private String bairro;
        private String complemento;
        private String cidade;
        private Estado estado;
        private String CEP;
        private Genero genero;
        private DenominacaoEtnica cor;
        private LocalDate dataCadastro;
        private Long numeroNIS;
        private boolean doadorSangue;
        private boolean doadorMedula;
        private boolean trabalhoEleitoral;
        private boolean jurado;
        public CandidatoDTO(String nome, LocalDate nascimento, String cpf, String emailPrincipal,
                        String telefonePrincipal, String rua, String numero, String bairro, String complemento,
                        String cidade, Estado estado, String cEP, Genero genero, DenominacaoEtnica cor,
                        LocalDate dataCadastro, Long numeroNIS, boolean doadorSangue, boolean doadorMedula,
                        boolean trabalhoEleitoral, boolean jurado) {
                this.nome = nome;
                this.nascimento = nascimento;
                this.cpf = cpf;
                this.emailPrincipal = emailPrincipal;
                this.telefonePrincipal = telefonePrincipal;
                this.rua = rua;
                this.numero = numero;
                this.bairro = bairro;
                this.complemento = complemento;
                this.cidade = cidade;
                this.estado = estado;
                CEP = cEP;
                this.genero = genero;
                this.cor = cor;
                this.dataCadastro = dataCadastro;
                this.numeroNIS = numeroNIS;
                this.doadorSangue = doadorSangue;
                this.doadorMedula = doadorMedula;
                this.trabalhoEleitoral = trabalhoEleitoral;
                this.jurado = jurado;
        }
        public String getNome() {
                return nome;
        }
        public void setNome(String nome) {
                this.nome = nome;
        }
        public LocalDate getNascimento() {
                return nascimento;
        }
        public void setNascimento(LocalDate nascimento) {
                this.nascimento = nascimento;
        }
        public String getCpf() {
                return cpf;
        }
        public void setCpf(String cpf) {
                this.cpf = cpf;
        }
        public String getEmailPrincipal() {
                return emailPrincipal;
        }
        public void setEmailPrincipal(String emailPrincipal) {
                this.emailPrincipal = emailPrincipal;
        }
        public String getTelefonePrincipal() {
                return telefonePrincipal;
        }
        public void setTelefonePrincipal(String telefonePrincipal) {
                this.telefonePrincipal = telefonePrincipal;
        }
        public String getRua() {
                return rua;
        }
        public void setRua(String rua) {
                this.rua = rua;
        }
        public String getNumero() {
                return numero;
        }
        public void setNumero(String numero) {
                this.numero = numero;
        }
        public String getBairro() {
                return bairro;
        }
        public void setBairro(String bairro) {
                this.bairro = bairro;
        }
        public String getComplemento() {
                return complemento;
        }
        public void setComplemento(String complemento) {
                this.complemento = complemento;
        }
        public String getCidade() {
                return cidade;
        }
        public void setCidade(String cidade) {
                this.cidade = cidade;
        }
        public Estado getEstado() {
                return estado;
        }
        public void setEstado(Estado estado) {
                this.estado = estado;
        }
        public String getCEP() {
                return CEP;
        }
        public void setCEP(String cEP) {
                CEP = cEP;
        }
        public Genero getGenero() {
                return genero;
        }
        public void setGenero(Genero genero) {
                this.genero = genero;
        }
        public DenominacaoEtnica getCor() {
                return cor;
        }
        public void setCor(DenominacaoEtnica cor) {
                this.cor = cor;
        }
        public LocalDate getDataCadastro() {
                return dataCadastro;
        }
        public void setDataCadastro(LocalDate dataCadastro) {
                this.dataCadastro = dataCadastro;
        }
        public Long getNumeroNIS() {
                return numeroNIS;
        }
        public void setNumeroNIS(Long numeroNIS) {
                this.numeroNIS = numeroNIS;
        }
        public boolean isDoadorSangue() {
                return doadorSangue;
        }
        public void setDoadorSangue(boolean doadorSangue) {
                this.doadorSangue = doadorSangue;
        }
        public boolean isDoadorMedula() {
                return doadorMedula;
        }
        public void setDoadorMedula(boolean doadorMedula) {
                this.doadorMedula = doadorMedula;
        }
        public boolean isTrabalhoEleitoral() {
                return trabalhoEleitoral;
        }
        public void setTrabalhoEleitoral(boolean trabalhoEleitoral) {
                this.trabalhoEleitoral = trabalhoEleitoral;
        }
        public boolean isJurado() {
                return jurado;
        }
        public void setJurado(boolean jurado) {
                this.jurado = jurado;
        }

        



}

