package br.dev.aao.syscertameapi.usuario;

public enum DenominacaoEtnica {
    AMARELO,
    BRANCO,
    INDIGINA,
    PARDO,
    PRETO
}
