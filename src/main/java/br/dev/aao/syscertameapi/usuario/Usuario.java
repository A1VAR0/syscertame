package br.dev.aao.syscertameapi.usuario;

import java.io.Serializable;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import jakarta.persistence.Entity;

@Entity
public class Usuario implements UserDetailsService, Serializable{
    private static final long serialVersionUID = 1L;
    private String nomeUsuario;
    private String password;
    private boolean contaNaoExpirada;
    private boolean contaNaoBloqueada;
    private boolean credenciaisNaoExpiradas;
    private boolean habilitado;

    

    
    public Usuario() {
    }


    public Usuario(String nomeUsuario, String password, boolean contaNaoExpirada, boolean contaNaoBloqueada,
            boolean credenciaisNaoExpiradas, boolean habilitado) {
        this.nomeUsuario = nomeUsuario;
        this.password = password;
        this.contaNaoExpirada = contaNaoExpirada;
        this.contaNaoBloqueada = contaNaoBloqueada;
        this.credenciaisNaoExpiradas = credenciaisNaoExpiradas;
        this.habilitado = habilitado;
    }


    public static long getSerialversionuid() {
        return serialVersionUID;
    }


    public String getNomeUsuario() {
        return nomeUsuario;
    }


    public void setNomeUsuario(String nomeUsuario) {
        this.nomeUsuario = nomeUsuario;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public boolean isContaNaoExpirada() {
        return contaNaoExpirada;
    }


    public void setContaNaoExpirada(boolean contaNaoExpirada) {
        this.contaNaoExpirada = contaNaoExpirada;
    }


    public boolean isContaNaoBloqueada() {
        return contaNaoBloqueada;
    }


    public void setContaNaoBloqueada(boolean contaNaoBloqueada) {
        this.contaNaoBloqueada = contaNaoBloqueada;
    }


    public boolean isCredenciaisNaoExpiradas() {
        return credenciaisNaoExpiradas;
    }


    public void setCredenciaisNaoExpiradas(boolean credenciaisNaoExpiradas) {
        this.credenciaisNaoExpiradas = credenciaisNaoExpiradas;
    }


    public boolean isHabilitado() {
        return habilitado;
    }


    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        throw new UnsupportedOperationException("Unimplemented method 'loadUserByUsername'");
    }

}
