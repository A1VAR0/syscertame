package br.dev.aao.syscertameapi.usuario.controller;

import org.springframework.web.bind.annotation.RestController;

import br.dev.aao.syscertameapi.usuario.Candidato;
import br.dev.aao.syscertameapi.usuario.dto.CandidatoDTO;
import br.dev.aao.syscertameapi.usuario.repository.service.CandidatoService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

@RestController
@RequestMapping("/api/candidato")
public class CandidatoController {
    @Autowired
    private CandidatoService candidatoService;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Candidato>> buscarTodos(){
        var todosCandidatos = candidatoService.buscarTodos();
        return ResponseEntity.ok(todosCandidatos);
    }

    @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Candidato> buscarCandidatoPorId(@PathVariable Long id){
        var candidato = candidatoService.buscarPorId(id);
        return ResponseEntity.ok(candidato);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Candidato> cadastrar(@RequestBody CandidatoDTO candidatoRecord){
        Candidato novoCandidato = candidatoService.cadastrar(candidatoRecord);
        return ResponseEntity.status(HttpStatusCode.valueOf(201)).body(novoCandidato);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletarCandidato(@PathVariable Long id){
        candidatoService.deletarCandidato(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Candidato> atualizarCandidato(@PathVariable Long id, @RequestBody CandidatoDTO candidato) {
        var candidatoAtualizado = candidatoService.atualizarCandidato(id, candidato);
        return ResponseEntity.ok(candidatoAtualizado);
    }
}
