package br.dev.aao.syscertameapi.endereco;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

@Embeddable
public class Endereco {
 

    /**
     * 
     */
    private String rua;

    /**
     * 
     */
    private String numero;

    /**
     * 
     */
    private String bairro;

    /**
     * 
     */
    private String complemento;

    /**
     * 
     */
    private String cidade;

    /**
     * 
     */
    @Enumerated(EnumType.STRING)
    private Estado estado;

    /**
     * formato nn.nnn-nnn
     */
    private String CEP;

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public String getCEP() {
        return CEP;
    }

    public void setCEP(String cEP) {
        CEP = cEP;
    }
    
}
