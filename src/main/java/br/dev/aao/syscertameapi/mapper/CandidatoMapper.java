package br.dev.aao.syscertameapi.mapper;


import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import br.dev.aao.syscertameapi.usuario.Candidato;
import br.dev.aao.syscertameapi.usuario.dto.CandidatoDTO;

@Mapper(componentModel = "spring")
public interface CandidatoMapper {
    CandidatoMapper INSTANCE = Mappers.getMapper(CandidatoMapper.class);

    @Mapping(target = "rua", source = "enderecoPrincipal.rua")
    @Mapping(target = "numero", source = "enderecoPrincipal.numero")
    @Mapping(target = "bairro", source = "enderecoPrincipal.bairro")
    @Mapping(target = "complemento", source = "enderecoPrincipal.complemento")
    @Mapping(target = "cidade", source = "enderecoPrincipal.cidade")
    @Mapping(target = "estado", source = "enderecoPrincipal.estado")
    @Mapping(target = "CEP", source = "enderecoPrincipal.CEP")
    CandidatoDTO map(Candidato source);

    @Mapping(target = "rua", source = "source.enderecoPrincipal.rua")
    @Mapping(target = "numero", source = "source.enderecoPrincipal.numero")
    @Mapping(target = "bairro", source = "source.enderecoPrincipal.bairro")
    @Mapping(target = "complemento", source = "source.enderecoPrincipal.complemento")
    @Mapping(target = "cidade", source = "source.enderecoPrincipal.cidade")
    @Mapping(target = "estado", source = "source.enderecoPrincipal.estado")
    @Mapping(target = "CEP", source = "source.enderecoPrincipal.CEP")
    CandidatoDTO mapAsWell(Candidato source);
    
    @InheritInverseConfiguration(name = "map")
    Candidato map(CandidatoDTO source); 
    
}
