package br.dev.aao.syscertameapi.exception;

import java.io.Serializable;
import java.time.LocalDateTime;

public class DominioException implements Serializable{
    private static final long serialVersionUID = 1L;
    private LocalDateTime timestamp;
    private String mensagem;
    private String detalhes;

    public DominioException(LocalDateTime timestamp, String mensagem, String detalhes) {
        this.timestamp = timestamp;
        this.mensagem = mensagem;
        this.detalhes = detalhes;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getDetalhes() {
        return detalhes;
    }

    public void setDetalhes(String detalhes) {
        this.detalhes = detalhes;
    }

    
}
