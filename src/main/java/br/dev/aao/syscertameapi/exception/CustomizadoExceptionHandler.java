package br.dev.aao.syscertameapi.exception;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
public class CustomizadoExceptionHandler extends ResponseEntityExceptionHandler{
    @ExceptionHandler(Exception.class)
    public final ResponseEntity<DominioException> handlerAllExceptions(Exception exception, WebRequest request){
        DominioException dominioException = new DominioException(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(dominioException, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(DataFuturaException.class)
    public final ResponseEntity<DominioException> handlerDataFuturaException(Exception exception, WebRequest request){
        DominioException dominioException = new DominioException(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(dominioException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(RecursoNaoEncontradoException.class)
    public final ResponseEntity<DominioException> handlerRecursoNaoEncontradoException(Exception exception, WebRequest request){
        DominioException dominioException = new DominioException(LocalDateTime.now(), exception.getMessage(), request.getDescription(false));
        return new ResponseEntity<>(dominioException, HttpStatus.NOT_FOUND);
    }
}
